# Choukette

[![build status](https://gitlab.com/choukette/choukette/badges/master/build.svg)](https://gitlab.com/choukette/choukette/commits/master)
[![coverage](https://gitlab.com/choukette/choukette/badges/master/coverage.svg?job=coverage)](https://choukette.gitlab.io/choukette/coverage)
[![PyPI version](https://badge.fury.io/py/choukette.svg)](https://badge.fury.io/py/choukette)

Choukette is a simple program that generate a badge in svg.

Need more info, look at the homepage documentation. [choukette.gitlab.io](http://choukette.gitlab.io/)

## Install

```
pip install choukette
```

## Quickstart

```
choukette [text] [value] [color]
```
